package hotel.entities;


import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.Spy;

import hotel.checkout.CheckoutCTL;
import hotel.checkout.CheckoutUI;
import hotel.credit.CreditCardType;

class CheckoutCTLTest {

	@Mock Hotel hotel;

	@Mock private double total;
	private int roomId = 12;
	@Mock private CheckoutUI checkoutUI;
	@Spy CheckoutCTL checkoutController = new CheckoutCTL(hotel);

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCreditDetailsEnteredNullException() {

		Executable e = () -> checkoutController.creditDetailsEntered(CreditCardType.MASTERCARD, 0, 0);
		assertThrows(Exception.class, e);

	}

	@Test
	void testRoomIdEntered() {
		checkoutController.run();

		assertNull(hotel.findActiveBookingByRoomId(1));

		hotel.addRoom(RoomType.DOUBLE, roomId);
		
		verify(hotel).findActiveBookingByRoomId(12);

		checkoutController.roomIdEntered(roomId);
		
		checkoutController.cancel();
		Executable e = () -> checkoutController.roomIdEntered(roomId);
		assertThrows(NullPointerException.class, e);
	}

}
