package hotel.entities;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages({"hotel.entities","hotel.checkin","hotel.service"})
public class AllTests {

}
