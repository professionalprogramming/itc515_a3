package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import hotel.credit.CreditCard;
import hotel.entities.Booking.State;



@ExtendWith(MockitoExtension.class)
class BookingTest {

	@Mock Hotel hotel;


	@Mock Guest guest;
	@Mock Room room;
	@Mock CreditCard creditCard;

	Date arrivalDate = new Date();
	int stayLength = 1;
	int numberOfOccupants = 1;

	@Spy @InjectMocks Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);


	@Test
	void bookingCheckinTest() {
		//Arrange
		when(booking.getState()).thenReturn(State.PENDING);
		assertTrue(booking.isPending());

		//Act
		booking.checkIn();

		//Assert
		verify(room).checkin();
		when(booking.getState()).thenCallRealMethod();
		assertTrue(booking.isCheckedIn());
	}

	@Test
	void bookingCheckoutTest() {
		//Arrange
		when(booking.getState()).thenReturn(State.CHECKED_IN);
		assertTrue(booking.isCheckedIn());

		//Act
		booking.checkOut();

		//Assert
		verify(room).checkout(booking);
		when(booking.getState()).thenCallRealMethod();
		assertTrue(booking.isCheckedOut());
	}

	@Test
	void bookingAddServiceChargeTest() {
		//Arrange
		assertEquals(booking.getCharges().size(), 0);

		//Act
		booking.addServiceCharge(ServiceType.BAR_FRIDGE, 20);
		booking.addServiceCharge(ServiceType.RESTAURANT, 40);
		booking.addServiceCharge(ServiceType.ROOM_SERVICE, 50);

		//Assert
		assertEquals(3, booking.getCharges().size());
	}

}
