package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;

@ExtendWith(MockitoExtension.class)
class HotelIntegrationTest {

	Hotel hotel = new Hotel();

	//Create a REAL booking
	@Mock Guest guest;
	Room room = new Room(0, RoomType.DOUBLE);
	@Mock CreditCard creditCard;
	Date arrivalDate = new Date();
	int stayLength = 1;
	int numberOfOccupants = 1;
	
	@Spy @InjectMocks Booking booking = new Booking(guest, room, arrivalDate, stayLength, numberOfOccupants, creditCard);
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testInvalidRoomIdAddServiceCharge(){
		int roomId = 1000000000;

		Executable test = () -> hotel.addServiceCharge(roomId, ServiceType.BAR_FRIDGE, 30);

		assertThrows(RuntimeException.class, test);
	}
	
	@Test
	void testServiceTypes()
	{
		int roomId = 0;
		hotel.activeBookingsByRoomId.put(0, booking);

		hotel.addServiceCharge(roomId, ServiceType.BAR_FRIDGE, 30);
		hotel.addServiceCharge(roomId, ServiceType.RESTAURANT, 30);
		hotel.addServiceCharge(roomId, ServiceType.ROOM_SERVICE, 30);

		verify(booking).addServiceCharge(ServiceType.BAR_FRIDGE, 30);
		verify(booking).addServiceCharge(ServiceType.RESTAURANT, 30);
		verify(booking).addServiceCharge(ServiceType.ROOM_SERVICE, 30);
	}

	@Test
	void testAddServiceForInvalidRoom(){
		int roomId = 0;
		hotel.activeBookingsByRoomId.put(0, booking);

		hotel.addServiceCharge(roomId, ServiceType.BAR_FRIDGE, 30);

		verify(booking).addServiceCharge(ServiceType.BAR_FRIDGE, 30);
	}
	
	@Test
	void testAllMethodsHotel(){
		room = new Room(0, RoomType.SINGLE);
		guest = new Guest("Barry", "White", 555555555);
		creditCard = new CreditCard(CreditCardType.MASTERCARD, 1, 1);
		
		Date arrivalDate = new GregorianCalendar(2018, 0, 1).getTime();
		int stayLength = 3;
		int occupantNumber = 1;
		long expected = 1020180;
		
		assertEquals(hotel.bookingsByConfirmationNumber.size(), 0);

		long confirmationNumber = hotel.book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard);

		assertEquals(expected, confirmationNumber);
		assertEquals(hotel.bookingsByConfirmationNumber.size(), 1);
	}
}
