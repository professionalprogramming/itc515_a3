package hotel.entities;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import java.util.Date;
import java.util.InputMismatchException;

import org.junit.jupiter.api.function.Executable;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;


import hotel.booking.BookingCTL;
import hotel.booking.BookingUI;
import hotel.checkout.CheckoutUI.State;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;

public class BookingCTLTest {
	
	@Mock Hotel hotel;
	@Mock Guest guest;
	@Mock Room room;
	
	@Mock BookingUI bookingUI;
	@Mock CreditCard creditCard;
	String existingName = "Ranjana";
	String existingAddress = "Sydney";
	int existingPhone = 555;
	
	CreditCardType type = CreditCardType.MASTERCARD;
	int ccv = 111;
	int accountNo = 123;
	
    @Spy @InjectMocks BookingCTL bookingController = new BookingCTL(hotel);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreditDetailsEnteredException() {
		Executable e = () -> bookingController.creditDetailsEntered(CreditCardType.MASTERCARD, 0, 0);
        assertThrows(NullPointerException.class, e);
        
        Executable e1 = () -> bookingController.creditDetailsEntered(null, 0, 0);
        assertThrows(NullPointerException.class, e1);
	}

	@Test
	public void testGuestDetailsEntered() {
		Executable e = () -> bookingController.guestDetailsEntered(null, null);
        assertThrows(RuntimeException.class, e);
	}
	
	@Test
	public void testBookingTimesEntered() {
		Executable e = () -> bookingController.bookingTimesEntered(new Date(), 0);
        assertThrows(RuntimeException.class, e);
        
        Executable e1 = () -> bookingController.bookingTimesEntered(null, 0);
        assertThrows(RuntimeException.class, e1);
        
        Executable e3 = () -> bookingController.bookingTimesEntered(new Date(1231231321), 99999);
        assertThrows(RuntimeException.class, e3);
	}
	
	@Test
	public void testPhoneNumberDetailsEntered() {
		Executable e = () -> bookingController.phoneNumberEntered(-1);
        assertThrows(RuntimeException.class, e);
        
        Executable e1 = () -> bookingController.phoneNumberEntered(999999);
        assertThrows(RuntimeException.class, e1);
        
        Executable e3 = () -> bookingController.phoneNumberEntered(1/0);
        assertThrows(RuntimeException.class, e3);
    }
	
//Need to Do
	@Test
	public void testRoomTypeAndOccupantsEntered() {
		Executable e = () -> bookingController.phoneNumberEntered(-1);
        assertThrows(RuntimeException.class, e);
        
        Executable e1 = () -> bookingController.phoneNumberEntered(999999);
        assertThrows(RuntimeException.class, e1);
        
        Executable e3 = () -> bookingController.phoneNumberEntered(1/0);
        assertThrows(RuntimeException.class, e3);
    }
	
	@Test
	public void testCreditDetailsEntered() {
		Executable e = () -> bookingController.phoneNumberEntered(-1);
        assertThrows(RuntimeException.class, e);
        
        Executable e1 = () -> bookingController.phoneNumberEntered(999999);
        assertThrows(RuntimeException.class, e1);
        
        Executable e3 = () -> bookingController.phoneNumberEntered(1/0);
        assertThrows(RuntimeException.class, e3);
    }
	
	@Test
    public void testRoomIdEntered() {
		bookingController.run();
        
        bookingController.creditDetailsEntered(type, accountNo, ccv);
        verify(bookingUI).displayMessage("This credit card is not authorised.");
        assertEquals(State.COMPLETED, bookingController.getState());
        
        bookingController.cancel();

		//Assert ---
		assertEquals(BookingCTL.State.CANCELLED, bookingController.getState());
		verify(bookingUI).displayMessage("Credit Card could not be authorized");
		verify(bookingUI).displayMessage("Booking cancelled");
		verify(hotel).isRegistered(existingPhone);
		verify(bookingUI).displayGuestDetails(existingName, existingAddress, existingPhone);
    }
}
