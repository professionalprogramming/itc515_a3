package hotel.entities;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import java.util.Date;

import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.aggregator.ArgumentAccessException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.credit.CreditCard;

@ExtendWith(MockitoExtension.class)
class HotelTest {

	Hotel hotel = new Hotel();
	
	@Mock Room room;
	@Mock Guest guest;
	@Mock CreditCard creditCard;
	
	@Mock Booking booking;
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCheckAvailableRoom() {
		int roomId = 11;
		hotel.addRoom(RoomType.SINGLE, roomId);
		
		assertTrue("Room Available", hotel.findAvailableRoom(RoomType.SINGLE, new Date(), 2) != null);
		assertFalse("Room Not Available", hotel.findAvailableRoom(RoomType.DOUBLE, new Date(), 2) != null);
	}
	
	@Test
	void testHotelException() {
		Executable e1 = () -> hotel.book(room, guest, new Date(), -1, 1, creditCard);
		assertThrows(ArgumentAccessException.class, e1);
		
		Executable e2 = () -> hotel.book(null, guest, new Date(), 1, 1, creditCard);
		assertThrows(Exception.class, e2);
		
		Executable e3 = () -> hotel.book(room, null, new Date(), 1, 1, creditCard);
		assertThrows(Exception.class, e3);
	}

	@Test
	void testBookingCheckedIn() {
		
		long dummyConfirmationNumber = 123222;
		assertFalse("This booking is not found", hotel.bookingsByConfirmationNumber.containsKey(dummyConfirmationNumber));
		
		long conFirmation_num = hotel.book(room, guest, new Date(), 2, 2, creditCard);
		assertTrue("Booking Created", hotel.bookingsByConfirmationNumber.containsKey(conFirmation_num));
		
		hotel.checkin(conFirmation_num);
		Booking bookingCreated = hotel.findBookingByConfirmationNumber(conFirmation_num);
		
		assertTrue("Room Checked in", bookingCreated.isCheckedIn());
		
		//Check the mocked booking using verify
		hotel.bookingsByConfirmationNumber.put(dummyConfirmationNumber, booking);
		hotel.checkin(dummyConfirmationNumber);

		verify(booking).checkIn();
	}
	
	@Test
	void testBookingCheckedOut() {
		Integer dummyRoomId = 12;
		assertFalse("Room Id not found", hotel.activeBookingsByRoomId.containsKey(dummyRoomId));
		
		hotel.activeBookingsByRoomId.put(dummyRoomId, booking);
		assertEquals(hotel.activeBookingsByRoomId.size(), 1);
		
		hotel.checkout(dummyRoomId);
		
		verify(booking).checkOut();
	}
	
	@Test
	void testAddServiceCharge() {
		Integer dummyRoomId = 12;
		hotel.activeBookingsByRoomId.put(dummyRoomId, booking);
		
		hotel.addServiceCharge(dummyRoomId, ServiceType.BAR_FRIDGE, 12);
		
		verify(booking).addServiceCharge(ServiceType.BAR_FRIDGE, 12);
	}
	
	@Test
	void testGuestIsRegistered() {
		hotel.registerGuest("Ranjana", "Sydney", 14);
		assertTrue("Guest registered",hotel.isRegistered(14));
		assertTrue("Guest found", hotel.findGuestByPhoneNumber(14) != null);
	}
}
